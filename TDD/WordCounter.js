module.exports = (input, delimiter = ' ') => {
  const words = input.split(delimiter).map(word => word.trim());

  return words.reduce((total, word) => {
    total[word] = total[word] ? total[word] + 1 : 1;

    return total;
  }, {});
};
