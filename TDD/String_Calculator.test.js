const add = require('./String_Calculator');

it('given empty string return 0', () => {
  const input = '';

  const output = add(input);

  expect(output).toEqual(0);
});

it('given "1" return 1', () => {
  const input = '1';

  const output = add(input);

  expect(output).toEqual(1);
});

it('given "1,2" return 2', () => {
  const input = '1, 2';

  const output = add(input);

  expect(output).toEqual(3);
});

it('given "1, 2, 3, 4, 5" return 15', () => {
  const input = '1, 2, 3, 4, 5';

  const output = add(input);

  expect(output).toEqual(15);
});

it('handle both new line and comma', () => {
  const input = '1\n2,3';

  const output = add(input);

  expect(output).toEqual(6);
});

it('custom delimiter', () => {
  const input = '//;\n1;2';

  const output = add(input);

  expect(output).toEqual(3);
});

it('show passed negatives', () => {
  const input = '1, -2, 3, 4, -5';

  try {
    add(input);
  } catch (error) {
    expect(error.message).toMatch('Negatives not allowed. Detected -2, -5');
  }
});
