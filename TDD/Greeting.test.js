const Greeting = require('./Greeting');

it('say hello to Bob', () => {
  const output = Greeting('Bob');

  expect(output).toEqual('Hello, Bob.');
});

it('say hello to Alice', () => {
  const output = Greeting('Alice');

  expect(output).toEqual('Hello, Alice.');
});

it('say hello to "my friend", when a name is not given', () => {
  const output = Greeting();

  expect(output).toEqual('Hello, my friend.');
});

it('shout back to the user', () => {
  const output = Greeting('JERRY');

  expect(output).toEqual('HELLO JERRY!');
});

it('say hello to 2 people when 2 names are given', () => {
  const output = Greeting(['Jill', 'Jane']);

  expect(output).toEqual('Hello, Jill and Jane.');
});

it('say hello to an arbitrary amount of names', () => {
  const output = Greeting(['Amy', 'Brian', 'Charlotte']);

  expect(output).toEqual('Hello, Amy, Brian, and Charlotte.');
});

it('say hello & shout at a bunch of people', () => {
  const output = Greeting(['Amy', 'BRIAN', 'Charlotte']);

  expect(output).toEqual('Hello, Amy and Charlotte. AND HELLO BRIAN!');
});

it('can handle comma-separated string', () => {
  const output = Greeting(['Bob', 'Charlie, Dianne']);

  expect(output).toEqual('Hello, Bob, Charlie, and Dianne.');
});

it('can handle intended comma-separated string', () => {
  const output = Greeting(['Bob', '"Charlie, Dianne"']);

  expect(output).toEqual('Hello, Bob and Charlie, Dianne.');
});
