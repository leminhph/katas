module.exports = function gcd(a, b) {
  if (a > b) {
    return gcd(b, a);
  }

  if (a !== 0) {
    return gcd(a, b % a);
  }

  return b;
};
