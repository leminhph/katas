const { FizzOrBuzz, generate } = require('./FizzBuzz');

describe('FizzBuzz', () => {
  describe('check if Fizz or Buzz', () => {
    it('given 1 return 1', () => {
      const output = FizzOrBuzz(1);

      expect(output).toEqual('1');
    });

    it('given 2 return 2', () => {
      const output = FizzOrBuzz(2);

      expect(output).toEqual('2');
    });

    it('given 3 return Fizz', () => {
      const output = FizzOrBuzz(3);

      expect(output).toEqual('Fizz');
    });

    it('given 5 return Buzz', () => {
      const output = FizzOrBuzz(5);

      expect(output).toEqual('Buzz');
    });

    it('given 6 return Fizz', () => {
      const output = FizzOrBuzz(6);

      expect(output).toEqual('Fizz');
    });

    it('given 10 return Buzz', () => {
      const output = FizzOrBuzz(10);

      expect(output).toEqual('Buzz');
    });

    it('given 13 return Fizz', () => {
      const output = FizzOrBuzz(13);

      expect(output).toEqual('Fizz');
    });

    it('given 13 return Fizz', () => {
      const output = FizzOrBuzz(13);

      expect(output).toEqual('Fizz');
    });

    it('given 15 return FizzBuzz', () => {
      const output = FizzOrBuzz(15);

      expect(output).toEqual('FizzBuzz');
    });

    it('given 52 return Buzz', () => {
      const output = FizzOrBuzz(52);

      expect(output).toEqual('Buzz');
    });
  });

  describe('correct sequence', () => {
    let output;

    beforeAll(() => {
      output = generate();
    });

    it('first two numbers are correct', () => {
      const sequence = output.slice(0, 2);

      expect(sequence).toEqual(['1', '2']);
    });

    it('number 3, 6, 9, 18, 31 are Fizz', () => {
      const numbers = [3, 6, 9, 18, 31];
      const indexes = numbers.map(number => number - 1);

      indexes.forEach((index) => {
        expect(output[index]).toEqual('Fizz');
      });
    });

    it('number 5, 10, 20, 52 are Buzz', () => {
      const numbers = [5, 10, 20, 52];
      const indexes = numbers.map(number => number - 1);

      indexes.forEach((index) => {
        expect(output[index]).toEqual('Buzz');
      });
    });

    it('number 15, 30, 45, 60 are FizzBuzz', () => {
      const numbers = [15, 30, 45, 60];
      const indexes = numbers.map(number => number - 1);

      indexes.forEach((index) => {
        expect(output[index]).toEqual('FizzBuzz');
      });
    });
  });
});
