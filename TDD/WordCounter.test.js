const WordCounter = require('./WordCounter');

describe('count correctly', () => {
  it('with "boom bang boom"', () => {
    const output = WordCounter('boom bang boom');

    expect(output).toEqual({
      boom: 2,
      bang: 1,
    });
  });

  it('with "boom bang boom boom"', () => {
    const output = WordCounter('boom bang boom boom');

    expect(output).toEqual({
      boom: 3,
      bang: 1,
    });
  });

  it('with "boom, bang, boom, boom"', () => {
    const output = WordCounter('boom, bang, boom, boom', ',');

    expect(output).toEqual({
      boom: 3,
      bang: 1,
    });
  });
});
