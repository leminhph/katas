const gcd = require('./GCD');

it('given 0, 1 return 1', () => {
  const input = [0, 1];

  const output = gcd(...input);

  expect(output).toEqual(1);
});

it('given 0, 2 return 2', () => {
  const input = [0, 2];

  const output = gcd(...input);

  expect(output).toEqual(2);
});

it('given 1, 2 return 1', () => {
  const input = [1, 2];

  const output = gcd(...input);

  expect(output).toEqual(1);
});

it('given 2, 4 return 2', () => {
  const input = [2, 4];

  const output = gcd(...input);

  expect(output).toEqual(2);
});

it('given 6, 9 return 3', () => {
  const input = [6, 9];

  const output = gcd(...input);

  expect(output).toEqual(3);
});

it('given 6, 15 return 3', () => {
  const input = [6, 15];

  const output = gcd(...input);

  expect(output).toEqual(3);
});

it('given 15, 27 return 3', () => {
  const input = [15, 27];

  const output = gcd(...input);

  expect(output).toEqual(3);
});

it('given 27, 15 return 3', () => {
  const input = [27, 15];

  const output = gcd(...input);

  expect(output).toEqual(3);
});
