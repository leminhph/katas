const FizzOrBuzz = (input) => {
  if (input % 15 === 0) {
    return 'FizzBuzz';
  }

  if (input % 3 === 0 || `${input}`.includes('3')) {
    return 'Fizz';
  }

  if (input % 5 === 0 || `${input}`.includes('5')) {
    return 'Buzz';
  }

  return `${input}`;
};

const generate = () => {
  return new Array(100)
    .fill(0)
    .map((_, index) => FizzOrBuzz(index + 1));
};

module.exports = {
  FizzOrBuzz,
  generate,
};
