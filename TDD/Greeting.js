const isShouting = person => person.toUpperCase() === person;
const flatten = array => array.reduce((acc, element) => acc.concat(element), []);
const intendedComma = name => name.startsWith('"') && name.endsWith('"');

module.exports = (name = 'my friend') => {
  if (Array.isArray(name)) {
    name = flatten(
      name.map(
        (person => (intendedComma(person) ? person.slice(1, person.length - 1) : person.split(',')
          .map(str => str.trim()))),
      ),
    );

    if (name.length > 2) {
      const shoutingPerson = name.find(isShouting);

      if (shoutingPerson) {
        name.splice(name.indexOf(shoutingPerson), 1);

        return `Hello, ${name.join(' and ')}. AND HELLO ${shoutingPerson}!`;
      }

      const lastPerson = name.pop();

      return `Hello, ${name.join(', ')}, and ${lastPerson}.`;
    }

    return `Hello, ${name.join(' and ')}.`;
  }

  if (isShouting(name)) {
    return `HELLO ${name}!`;
  }

  return `Hello, ${name}.`;
};
