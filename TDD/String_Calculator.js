module.exports = (input) => {
  if (!input) {
    return 0;
  }

  let delimiter = ',';

  if (input.startsWith('//')) {
    delimiter = input.charAt(2);
    input = input.slice(4);
  }

  const numbers = input
    .replace('\n', delimiter)
    .split(delimiter)
    .map(number => parseInt(number, 10));

  const negatives = numbers.filter(number => number < 0);

  if (negatives.length > 0) {
    throw new Error(
      `Negatives not allowed. Detected ${negatives.join(', ')}`,
    );
  }

  return numbers.reduce((sum, number) => sum + number, 0);
};
